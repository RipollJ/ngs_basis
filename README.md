# NGS basis

Here, you can find :
- tutorials for diverses NGS analyses
- talks presentation realized in seminaries

This project is provided as is, the aim is to help students or newbies to better
understand how to analyses their NGS data

Any suggestions or contributions are welcome.

If you use provided codes, don't forget to cite the different programs used.

---------------------------------------------------------------------------

See the [LICENSE](https://gitlab.com/RipollJ/ngs_basis/Licence.md) file for license rights and limitations (MIT).